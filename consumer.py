#!/usr/bin/env python
# coding: utf8

"""
Consumer
"""
import os
import sys
import json
import logging
from optparse import OptionParser

BASE_DIR = os.path.dirname(os.path.realpath(__file__ + "/../"))
sys.path.append(BASE_DIR)

from src import *
from handler import echo, mongo, rabbit, redis

def main():
    """ main function to start a consumer """
    LOG_FORMAT = ('%(levelname) -5s %(asctime)s %(name) -10s %(funcName) '
                  '-15s %(lineno) -5d: %(message)s')
    # option parser
    parser = OptionParser()
    parser.add_option("-c", "--conf", dest="configPath",
                      help="Read configuration from FILE", metavar="FILE")
    parser.add_option("-n", "--name", dest="name",
                      help="The name of the consumer.")
    (options, args) = parser.parse_args()

    configPath = options.configPath
    name = options.name

    if None == configPath:
        print('No configpath set.')
        sys.exit(1)

    with open(configPath, "r") as f:
        config = json.load(f)
        if name:
            config['name'] = name

    if None == config["consumer"]['handler'] or {} == config["consumer"]['handler']:
        print('No handler set.')
        sys.exit(1)

    if config["consumer"]['handler']['name'] == 'echo':
        handle = echo.MsgHandler(config["consumer"]['handler'])
    elif config["consumer"]['handler']['name'] == 'mongo':
        handle = mongo.MongoHandler(config["consumer"]['handler'])
    elif config["consumer"]['handler']['name'] == 'rabbit':
        handle = rabbit.RabbitHandler(config["consumer"]['handler'])
    elif config["consumer"]['handler']['name'] == 'redis':
        handle = redis.RedisHandler(config["consumer"]['handler'])

    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    logger = logging.getLogger()

    try:        
        config["consumer"]['handler'] = handle
        config['logger'] = logger
        consumer = consume.Consumer(config)
        try:
            consumer.run()
        except KeyboardInterrupt:
            consumer.stop()
    except Exception as error:
        logger.info("Error: %s" % (error, ))

if __name__ == '__main__':
    main()
