# rabbitmq-python

Python scripts to work with RabbitMQ and JSON messages.  
Consumer and Publisher are common classes which can be used with different configurations.  

### Docker

The repositiry includes a `docker-compose` file to run RabbitMQ, Redis and MongoDB.

    $ docker-compose up

### Configuration

```javascript
{
	"publisher": {
		"app_id": "",
        "amqp_url": "amqp://rabbit:rabbit@127.0.0.1:5672/myVhost",
		"exchange_name": "pika-direct",
	    "exchange_type": "direct",
		"routing_keys": "direct-queue",
		"delivery_mode": 1,
		"reply_to": ""
	},
	"consumer": {
		"name": "",
		"amqp_url": "amqp://rabbit:rabbit@127.0.0.1:5672/myVhost",
		"exchange_name": "pika-direct",
	    "exchange_type": "direct",
		"routing_keys": "direct-queue",
        "queue": "direct-queue",
		"consumer_tag": "consumer",		
		"prefetch_count": 10,
		"durable": true,
		"exclusive": false,
	    "auto_delete": false,
		"arguments": {},
		"handler": {
			"name": "echo"
		}
	}
}
```

__consumer_tag__ - Identifier for the consumer.  
__durable__ - Durable queues remain active when a server restarts.  
__exclusive__ - Exclusive queues are for the current connection, deleted when that connection closes.  
__auto_delete__ - Queue is deleted when all consumers have closed their connections.  
__arguments__ - Extra key/value arguments for the queue like `x-dead-letter-exchange` or `x-message-ttl`.  

The consumer can get different handler to process the incoming messages.  
After processing the handler are responsible to acknowledge or reject the messages.  

Following handler, as an example, are present:  

### echo  
The echo handler only responses the message.  
```javascript
"handler": {
    "name": "echo"
}
```

### mongo  
The mongo handler store the message in a collection.  
```javascript
"handler": {
    "name": "mongo",
    "mongo_url": "mongodb://127.0.0.1:27017/",
    "mongo_db": "rabbit", 
    "mongo_collection": "events"
}
```

### rabbit  
The rabbit handler publish the message to an other exchange.  
```javascript
"handler": {
    "name": "rabbit",
    "amqp_url": "amqp://guest:guest@127.0.0.1:5672/",
    "exchange": "repeater",
    "routing_key": "example.route"
}
```

### redis  
The redis handler store the message as key value.  
```javascript
"handler": {
    "name": "redis",
    "redis_host": "127.0.0.1",
    "redis_port": "6379", 
    "redis_db": "0"
}
```

## Examples

Cretae a virtual environment.

    $ python3 -m venv .
    $ source bin/activate
    $ pip3 install -r requirements.txt

There are several configuraion examples inside of the `conf` directory.  

### direct

Create a consumer and an publisher which communicate direct with each other.

    $ python consumer.py -c ./conf/direct.json

    $ python publisher.py -c ./conf/direct.json

A second publisher will work like expected and publish messages same like the first one.

    $ python publisher.py -c ./conf/direct.json

A second consumer will seperate the work, the sent messages get seperated between both consumer.

    $ python consumer.py -c ./conf/direct.json

### fanout

Create two consumer which receive both the messages from the publisher.

    $ python consumer.py -c ./conf/fanout_queue1.json
    $ python consumer.py -c ./conf/fanout_queue2.json

    $ python publisher.py -c ./conf/fanout.json

A second publisher will work like expected and publish messages same like the first one.

    $ python publisher.py -c ./conf/fanout.json

A third consumer will act like expected, all consumer get the messages sent from the publisher.  
The configuration for the third consumer includes a rabbit handler so an other consumer should be started.

    $ python consumer.py -c ./conf/fanout_queue3.json
    $ python consumer.py -c ./conf/repeater_consume.json

### topic

Create a consumer which listen to all messages and one which only listen to messages with routing keys like user.*.

    $ python consumer.py -c ./conf/topic_all.json
    $ python consumer.py -c ./conf/topic_user.json

    $ python publisher.py -c ./conf/topic_all.json
    $ python publisher.py -c ./conf/topic_user.json
    $ python publisher.py -c ./conf/topic_user_action.json

The configuration includes a mongo handler.

    $ python consumer.py -c ./conf/topic_user_action.json

## Testing

    $ pytest --flakes --cov src/ --cov-report html

## Feedback
Star this repo if you found it useful. Use the github issue tracker to give feedback on this repo.