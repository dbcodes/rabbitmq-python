#!/usr/bin/env python
# coding: utf8
"""
Publish test
"""
import os
import sys
import unittest
from mock import Mock
from pika import BlockingConnection
from pika.channel import Channel

BASEDIR = os.path.dirname(os.path.realpath(__file__ + "/../"))
sys.path.append(BASEDIR)

from src import publish

class PublishTestCase(unittest.TestCase):

    def setUp(self):
        """Create some of the stuff we will be using throughout"""
        self.logger_mock = Mock()
        self.pub_config = {
            "logger": self.logger_mock,
            "publisher": {
                "app_id": "", 
                "amqp_url": "amqp://rabbit:rabbit@127.0.0.1:5672/myVhost",
                "exchange_name": "pika-direct",
                "exchange_type": "direct",
                "routing_keys": "example.text",
                "delivery_mode": 1,
		        "reply_to": ""
            }
        }
        self.connection = Mock(spec=BlockingConnection)
        self.publisher = publish.Publisher(self.pub_config)
        self.publisher.connection = self.connection
        self.publisher.channel = Mock()
        self.publisher.channel.basic_publish = Mock(return_value=3)

    def test_create_publisher(self):
        """test"""
        self.assertTrue(isinstance(self.publisher, publish.Publisher))

    def test_send(self):
        """ Test basic_publish and exchange_declare are properly called"""
        self.publisher.send('{}')
        self.logger_mock.info.assert_called_with("Send: {} Response 3")

    def test_close(self):
        """test"""
        channel_mock = Mock(spec=Channel)
        self.connection.channel.return_value = channel_mock

        self.publisher.close()
        self.assertTrue(isinstance(self.publisher, publish.Publisher))

