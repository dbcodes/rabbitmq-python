#!/usr/bin/env python
# coding: utf8

import os
import sys
import unittest
from mock import Mock
from pika.channel import Channel
from pika.frame import Frame

baseDir = os.path.dirname(os.path.realpath(__file__ + "/../"))
sys.path.append(baseDir)

from src import consume

class ConsumeTestCase(unittest.TestCase):

    def setUp(self):
        """Create some of the stuff we will be using throughout"""
        self.logger_mock = Mock()
        self.consumer_config = {
            "logger": self.logger_mock,
            "consumer": {
                "name": "",
                "amqp_url": "amqp://rabbit:rabbit@127.0.0.1:5672/myVhost",
                "exchange_name": "pika-direct",
                "exchange_type": "direct",
                "routing_keys": "direct-queue",
                "consumer_tag": "test",
                "queue": "test-queue",
                "prefetch_count": 10,
                "durable": True,
                "exclusive": False,
                "auto_delete": False,
                "arguments": {},
                "handler": {
                    "name": "echo"
                }
            }
        }
        self.consumer = consume.Consumer(self.consumer_config)

    def test_create_consumer(self):
        self.assertTrue(isinstance(self.consumer, consume.Consumer))
        self.assertEqual(self.consumer._connection, None)
        self.assertEqual(self.consumer._channel, None)
        self.assertEqual(self.consumer._closing, False)
        self.assertEqual(self.consumer._consumer_tag, 'test')        

    def test_on_channel_open(self):
        channel_mock = Mock(spec=Channel)
        self.consumer.on_channel_open(channel_mock)
        channel_mock.add_on_close_callback.assert_called_with(self.consumer.on_channel_closed)

    def test_exchange_declare(self):
        channel_mock = Mock(spec=Channel)                
        self.consumer.on_channel_open(channel_mock)
        channel_mock.exchange_declare.assert_called_with(self.consumer.on_exchange_declareok, exchange='pika-direct', exchange_type='direct')

    def test_queue_declare(self):
        channel_mock = Mock(spec=Channel)                
        self.consumer.on_channel_open(channel_mock)
        frame_mock1   = Mock(spec=Frame)
        frame_mock2   = Mock(spec=Frame)
        self.consumer.on_exchange_declareok(frame_mock1)
        self.consumer._on_queue_declareok(frame_mock2)
        channel_mock.queue_declare.assert_called_with(callback=self.consumer._on_queue_declareok,
                                                      queue="test-queue",
                                                      durable=True,
                                                      exclusive=False,
                                                      auto_delete=False,
                                                      arguments={})
        channel_mock.queue_bind.assert_called_with(self.consumer._on_bindok, 'test-queue', 'pika-direct', 'direct-queue')

    def test_on_queue_declared_with_routing_keys(self):
        channel_mock = Mock(spec=Channel)
        self.consumer.on_channel_open(channel=channel_mock)
        #channel_mock.queue_bind.assert_has_call(call(self.consumer._on_bindok, "test-queue", "pika-direct", "example.text"))

    def test_on_exchange_bound(self):
        channel_mock = Mock(spec=Channel)
        frame_mock   = Mock(spec=Frame)        
        self.consumer.on_channel_open(channel=channel_mock)
        self.consumer._on_bindok(unused_frame=frame_mock)
        channel_mock.basic_consume.assert_called_with(self.consumer.on_message, consumer_tag='test', queue="test-queue")








