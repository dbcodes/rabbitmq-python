#!/usr/bin/env python
# coding: utf8

"""
Publisher
"""
import os
import sys
import json
import logging
from optparse import OptionParser

baseDir = os.path.dirname(os.path.realpath(__file__ + "/../"))
sys.path.append(baseDir)

from src import *

def main():
    """ main function """
    LOG_FORMAT = ('%(levelname) -5s %(asctime)s %(name) -10s %(funcName) '
                  '-15s %(lineno) -5d: %(message)s')
    # option parser
    parser = OptionParser()
    parser.add_option("-c", "--conf", dest="configPath",
                      help="Read configuration from FILE", metavar="FILE")
    parser.add_option("-n", "--name", dest="name",
                      help="The name of the publisher.")
    (options, args) = parser.parse_args()

    configPath = options.configPath
    name = options.name

    if None == configPath:
        print('No configpath set.')
        sys.exit(1)

    with open(configPath, "r") as f:
        config = json.load(f)
        if name:
            config['name'] = name

    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    logger = logging.getLogger()

    try:
        config['logger'] = logger
        publisher = publish.Publisher(config)
        publisher.connect()
        try:
            i = 1
            while True:
                message = '{"id":"' + str(i) +'", "msg": "message ' + str(i) +'"}'
                publisher.send(message)
                i += 1
        except KeyboardInterrupt:
            publisher.close()
    except Exception as error:
        print("Error: %s" % (error, ))


if __name__ == '__main__':
    main()
