#!/usr/bin/env python
# coding: utf8

"""
MainHandler
"""
import time
import json

def validate(msg):
    """
    Validate the message.
    """
    json_msg = json.loads(msg)
    json_msg['ts'] = int(time.time())
    return json.dumps(json_msg)

class MainHandler(object):
    """
    Main handler
    """

    def __init__(self, conf):
        """
        init
        """
        self.conf = conf