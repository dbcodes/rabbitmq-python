#!/usr/bin/env python
# coding: utf8

"""
RedisHandler
"""     
import os
import sys
import redis

BASE_DIR = os.path.dirname(os.path.realpath(__file__ + "/./"))
sys.path.append(BASE_DIR)

import main

class RedisHandler(main.MainHandler):
    """
    Handler to write messages into a Redis.
    """

    def __init__(self, config):
        """
        init
        """
        try:
            rclient = redis.Redis(host=config['redis_host'],
                                  port=config['redis_port'],
                                  db=config['redis_db'])
            self._rclient = rclient
        except Exception as error:
            raise error

    def handle(self, channel, basic_deliver, properties, body):
        """
        insert message
        """
        data = json.loads(body)
        self._rclient.set(data['id'], data['id'])
        channel.basic_ack(basic_deliver.delivery_tag)
        #channel.basic_reject(delivery_tag=basic_deliver.delivery_tag, requeue=True)
        return 'Received message # %s from %s: %s', (basic_deliver.delivery_tag,    
                                                     properties.app_id, body)
