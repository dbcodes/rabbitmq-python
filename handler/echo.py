#!/usr/bin/env python
# coding: utf8

"""
MsgHandler
"""
import os
import sys

BASE_DIR = os.path.dirname(os.path.realpath(__file__ + "/./"))
sys.path.append(BASE_DIR)

import main

class MsgHandler(main.MainHandler):
    """
    Simple handler
    """

    def handle(self, channel, basic_deliver, properties, body):
        """
        Print message
        """
        body = main.validate(body)
        channel.basic_ack(basic_deliver.delivery_tag)
        #channel.basic_reject(delivery_tag=basic_deliver.delivery_tag, requeue=True)
        return 'Received message # {} from {} {}: {}'.format(basic_deliver.delivery_tag,
                                                             basic_deliver.routing_key,
                                                             properties.app_id,
                                                             body)
