#!/usr/bin/env python
# coding: utf8

"""
RabbitHandler
"""
import os
import sys
import pika

BASE_DIR = os.path.dirname(os.path.realpath(__file__ + "/./"))
sys.path.append(BASE_DIR)

import main

class RabbitHandler(main.MainHandler):
    """
    Handler to publish messages to RabbitMQ.
    """

    def __init__(self, config):
        """
        init
        """
        self._url = config['amqp_url']
        self._exchange = config['exchange']
        self._routing_key = config['routing_key']
        self._queue = config['queue']
        try:
            self.connection = pika.BlockingConnection(
                pika.URLParameters(self._url)
            )
            self.channel = self.connection.channel()
            self.channel.exchange_declare(exchange=self._exchange,
                                          type="direct")
            #self.channel.queue_declare(queue=self._queue)
        except pika.exceptions.AMQPConnectionError as error:
            raise error

    def handle(self, channel, basic_deliver, properties, body):
        """
        publish message
        """
        self.channel.basic_publish(exchange=self._exchange,
                                   routing_key=self._routing_key,
                                   body=body)
        channel.basic_ack(basic_deliver.delivery_tag)
        #channel.basic_reject(delivery_tag=basic_deliver.delivery_tag, requeue=True)
        return 'Received message # %s from %s: %s', (basic_deliver.delivery_tag,
                                                     properties.app_id, body)
