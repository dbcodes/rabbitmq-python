#!/usr/bin/env python
# coding: utf8

"""
MongoHandler
"""
import os
import sys
import json
import pymongo
from pymongo import MongoClient

BASE_DIR = os.path.dirname(os.path.realpath(__file__ + "/./"))
sys.path.append(BASE_DIR)

import main

class MongoHandler(main.MainHandler):
    """
    Handler to write messages as documents into a MongoDB collection.
    """

    def __init__(self, config):
        """
        Connect to server, select database and collection.
        """
        try:
            client = MongoClient(config['mongo_url'])
            self._db = client[config['mongo_db']]
            self._collection = self._db[config['mongo_collection']]
        except pymongo.errors.ConnectionFailure as error:
            raise error

    def handle(self, channel, basic_deliver, properties, body):
        """
        Insert message as document into collection.
        """
        body = main.validate(body)
        json_data = json.loads(body)
        json_data['exchange'] = basic_deliver.exchange
        json_data['routing_key'] = basic_deliver.routing_key
        insert = self._collection.insert(json_data)
        channel.basic_ack(basic_deliver.delivery_tag)
        #channel.basic_reject(delivery_tag=basic_deliver.delivery_tag, requeue=True)
        return 'Insert document with ObjectId: %s %s %s'% (str(insert), basic_deliver.delivery_tag, properties.app_id)
