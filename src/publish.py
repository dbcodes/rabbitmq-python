#!/usr/bin/env python
# coding: utf8

"""
Publisher
"""
import time
import pika

class Publisher(object):
    """
    Publisher
    """
    def __init__(self, config):
        """
        Initalize publisher

        :param dict config: Configuration dict
        """
        self._logger = config["logger"]
        self._conf = config["publisher"]
        self.connection = None
        self.channel = None

    def connect(self):
        """
        Create connection, create channel and declare exchange.
        """
        self._logger.info('Connecting to {}'.format(self._conf["amqp_url"]))
        self.connection = pika.BlockingConnection(pika.URLParameters(self._conf["amqp_url"]))
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange=self._conf["exchange_name"],
                                      exchange_type=self._conf["exchange_type"])

    def send(self, msg):
        """
        Send a message to RabbitMQ.

        :param str msg: message to send
        """
        properties = pika.BasicProperties(app_id=self._conf["app_id"],
                                          delivery_mode=self._conf["delivery_mode"],
                                          reply_to=self._conf["reply_to"],
                                          content_type='text/plain',
                                          timestamp=int(time.time()))
        res = self.channel.basic_publish(exchange=self._conf["exchange_name"],
                                         routing_key=self._conf["routing_keys"],
                                         body=msg,
                                         properties=properties,
                                         mandatory=False)
        self._logger.info('Send: {} Response {}'.format(msg, res))

    def close(self):
        """
        Close the connection.
        """
        self.connection.close()
        self._logger.info('Close connection')
