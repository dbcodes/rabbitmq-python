#!/usr/bin/env python
# coding: utf8

"""
Consumer
"""
import time
import pika


class Consumer(object):
    """
    General consumer object
    """

    def __init__(self, config):
        """
        Creates a new instance of the consumer class.

        :param dict config: Configuration dictionary
        """
        self._connection = None
        self._channel = None
        self._closing = False
        self.log = config["logger"]
        self._conf = config["consumer"]
        self._consumer_tag = self._conf["consumer_tag"]

    def connect(self):
        """
        Connects to RabbitMQ, returning the connection handle.
        When the connection is established, the `_on_connection_open` method
        will be invoked.
        """
        self.log.info('Connecting to {}'.format(self._conf['amqp_url']))
        return pika.SelectConnection(pika.URLParameters(self._conf['amqp_url']),
                                     self._on_connection_open,
                                     stop_ioloop_on_close=False)

    def _on_connection_open(self, connection):
        """
        This method is called by pika once the connection to RabbitMQ has
        been established.

        :param object connection: Connection object
        """
        self.log.info('Connection opened {}'.format(self._conf['name']))
        self._add_on_connection_close_cb()
        self.open_channel()

    def _add_on_connection_close_cb(self):
        """
        Adds an on close callback that will be invoked by pika
        when RabbitMQ closes the connection.
        """
        self.log.info('Add connection close cb {}'.format(self._conf['name']))
        self._connection.add_on_close_callback(self.on_connection_closed)

    def on_connection_closed(self, channel, reply_code, reply_text):
        """
        Callback function called when connection gets closed.

        :param object channel: Channel object  
        :param int reply_code: the reply code  
        :param str replt_text: the reply text  
        """
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            self.log.warning('Connection reopen in 5 secs: ({}) {}'.format(reply_code, reply_text))
            self._connection.add_timeout(5, self.reconnect)

    def reconnect(self):
        """ Reconnects to RabbitMQ. """
        self.log.info('Reconnect')
        self._connection.ioloop.stop()
        if not self._closing:
            self._connection = self.connect()
            self._connection.ioloop.start()

    def open_channel(self):
        """ Opens a channel on RabbitMQ. """
        self.log.info('Creating a new channel {}'.format(self._conf['name']))
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """
        Callback function called when a channel gets open.

        :param object channel: Channel object
        """
        self.log.info('Channel opened {}'.format(self._conf['name']))
        self._channel = channel
        self._add_on_channel_close_callback()
        self.setup_exchange(self._conf['exchange_name'], self._conf['exchange_type'])

    def _add_on_channel_close_callback(self):
        """
        Adds an on close callback that will be invoked by pika
        when RabbitMQ closes the channel.
        """
        self.log.info('Adding channel close callback {}'.format(self._conf['name']))
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reply_code, reply_text):
        """
        Callback function called when a channel gets closed.

        :param object channel: Channel object  
        :param int reply_code: the reply code  
        :param str replt_text: the reply text  
        """
        self.log.warning('Channel {} was closed: ({}) {}'.format(channel, reply_code, reply_text))
        self._connection.close()

    def setup_exchange(self, exchange_name, exchange_type):
        """
        Declaring an exchange.

        :param str exchange_name: Name for the exchange  
        :param str exchange_type: Type of the exchange  
        """
        self.log.info('Declaring exchange {} {}'.format(exchange_name, self._conf['name']))
        self._channel.exchange_declare(self.on_exchange_declareok,
                                       exchange=exchange_name,
                                       exchange_type=exchange_type)
    
    def on_exchange_declareok(self, unused_frame):
        """
        Callback function called when the exchange is declared.

        :param object unused_frame: The unused frame
        """
        self.log.info('Exchange declared')
        self.setup_queue(self._conf['queue'])

    def setup_queue(self, queue_name):
        """
        Declare a queue, creates if needed.
        
        :param str queue_name: Name of the queue
        """
        self.log.info('Declaring queue {} {}'.format(queue_name, self._conf['name']))
        self._channel.queue_declare(callback=self._on_queue_declareok,
                                    queue=queue_name,
                                    durable=self._conf['durable'],
                                    exclusive=self._conf['exclusive'],
                                    auto_delete=self._conf['auto_delete'],
                                    arguments=self._conf['arguments'])
    
    def _on_queue_declareok(self, method_frame):
        """
        Callback for queue_declare to bind queue.

        :param object method_frame: Method frame
        """
        self.log.info('Binding {} to {} with {}'.format(self._conf['exchange_name'],
                                                        self._conf['queue'],
                                                        self._conf['routing_keys']))
        self._channel.queue_bind(self._on_bindok,
                                 self._conf['queue'],
                                 self._conf['exchange_name'],
                                 self._conf['routing_keys'])
    def _on_bindok(self, unused_frame):
        """
        Callback function called when queue gots bound.

        :param object unused_frame: The unused frame
        """
        self.log.info('Queue bound')
        self.start_consuming()

    def start_consuming(self):
        """
        Define consumer_tag and consume messages.
        """
        self.log.info('Issuing consumer related RPC commands')
        self.add_on_cancel_callback()
        self._channel.basic_qos(prefetch_count=self._conf['prefetch_count'])
        self._consumer_tag = self._channel.basic_consume(
            self.on_message,
            queue=self._conf['queue'],
            consumer_tag=self._consumer_tag
        )

    def add_on_cancel_callback(self):
        """
        Adds an on cancel callback that will be invoked by pika
        when RabbitMQ cancel the channel.
        """
        self.log.info('Adding consumer cancellation callback')
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        """
        Callback function called when channel gets cancelled.

        :param object method_frame: Method frame
        """
        self.log.info('Consumer cancelled remotely, go down: {}'.format(method_frame))
        if self._channel:
            self._channel.close()

    def on_message(self, channel, method, header, body):
        """
        Handles the incomming messages with the configured handler.

        :param object channel: Channel object  
        :param object method: Method object  
        :param object header: Header  
        :param object body: Body  
        """
        start_time = time.time()
        if self._conf['handler']:
            msg = self._conf['handler'].handle(self._channel, method, header, body)
            end_time = time.time()
            duration = (end_time - start_time) * 1000.0
            self.log.info('Handler response # {} in {}'.format(msg, duration))
        else:
            self.log.info('Received message # {} from {}: {}'.format(method.delivery_tag, header, body))

    def stop_consuming(self):
        """
        Cancel channel when present.
        """
        if self._channel:
            self.log.info('Sending a Basic.Cancel RPC command')
            self._channel.basic_cancel(self.on_cancelok, self._consumer_tag)

    def on_cancelok(self, unused_frame):
        """
        Callback function called when the channel is cancelled.

        :param object unused_frame: The unused frame
        """
        self.log.info('Acknowledged the cancellation of consumer')
        self.close_channel()

    def close_channel(self):
        """
        Close the channel.
        """
        self.log.info('Closing the channel')
        self._channel.close()

    def run(self):
        """
        Connect to RabbitMQ and starting the pika ioloop.
        """
        self._connection = self.connect()
        self._connection.ioloop.start()

    def stop(self):
        """
        Stop consuming and stop the ioloop.
        """
        self.log.info('Stopping')
        self._closing = True
        self.stop_consuming()
        self._connection.ioloop.stop()
        self.log.info('Stopped')
